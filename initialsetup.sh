#!/bin/bash

function isRoot() {
	if [ "$EUID" -ne 0 ]; then
		return 1
	fi
}

function checkOS() {
	source /etc/os-release
	if [[ $ID != "debian" && $VERSION_ID != 11 ]]; then
		return 1
	fi
}

function initialCheck() {
	if ! isRoot; then
		echo ""
		echo -e "Пожалуйста, запустите этот скрипт как root!"
		echo ""
		exit 1
	fi
	if ! checkOS; then
		echo ""
		echo -e "Ваша операционная система система не поддерживается!"
		echo -e "Скрипт создан для ОС Debian 11!"
		echo ""
		exit 1
	fi
}

function installQuestions() {
	clear
	echo ""
	echo -e "Добро пожаловать в скрипт для начальной настройки сервера!"
	echo -e "Пожалуйста, ответьте на следующие вопросы, чтобы продолжить."
	echo ""

	WAN_IP=$(ip -4 addr | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p' | awk '{print $1}' | head -1)
	read -rp "Введите IP Вашего сервера: " -e -i "${WAN_IP}" WAN_IP

	WAN=$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)
	read -rp "Введите название сетевого интерфейса Вашего сервера: " -e -i "${WAN}" WAN

	until [[ ${WAN_PORT} =~ ^[0-9]+$ ]] && [ "${WAN_PORT}" -ge 1 ] && [ "${WAN_PORT}" -le 65535 ]; do
	read -rp "Введите новый порт для Вашего сервера [1-65535]: " -e -i "25431" WAN_PORT
	done

	NEW_USER_NAME=$(</dev/urandom tr -dc 'A-Za-z0-9' | head -c 12)
	read -rp "Введите имя для нового пользователя: " -e -i "${NEW_USER_NAME}" NEW_USER_NAME

	NEW_USER_PASSWORD=$(</dev/urandom tr -dc 'A-Za-z0-9' | head -c 24)
	read -rp "Введите пароль для нового пользователя: " -e -i "${NEW_USER_PASSWORD}" NEW_USER_PASSWORD

	read -rp "Введите Ваш SSH ключ: " -e -i "${USER_SSH_KEY}" USER_SSH_KEY
}

function addSSHkey() {
	read -rp "Введите новый SSH ключ: " -e -i "${USER_NEW_SSH_KEY}" USER_NEW_SSH_KEY
	SSHkeyUser=$(members initialsetup)
	echo "$USER_NEW_SSH_KEY" >> /home/$SSHkeyUser/.ssh/authorized_keys
}

function serverSetup() {
	installQuestions

	# Первичная настройка локалей
	export LC_CTYPE=en_US.UTF-8
	export LC_ALL=en_US.UTF-8
	source ~/.bashrc

	# Обновление системы
	apt update
	apt -y full-upgrade
	apt -y autoremove
	apt clean

	# Настройка SSH
	echo "#	$OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

Include /etc/ssh/sshd_config.d/*.conf

Port $WAN_PORT
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
#LogLevel INFO

# Authentication:

#LoginGraceTime 2m
PermitRootLogin no
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

#PubkeyAuthentication yes

# Expect .ssh/authorized_keys2 to be disregarded by default in future.
#AuthorizedKeysFile	.ssh/authorized_keys .ssh/authorized_keys2

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no
#PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no

# GSSAPI options
GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM yes

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
PrintMotd no
#PrintLastLog yes
#TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# Allow client to pass locale environment variables
#AcceptEnv LANG LC_*

# override default of no subsystems
Subsystem	sftp	/usr/lib/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#	X11Forwarding no
#	AllowTcpForwarding no
#	PermitTTY no
#	ForceCommand cvs server" > /etc/ssh/sshd_config
	systemctl restart sshd

	# Добавление нового пользователя
	groupadd initialsetup
	useradd -G sudo,initialsetup -m -p $(perl -e 'print crypt($ARGV[0], "password")' $NEW_USER_PASSWORD) -s /bin/bash $NEW_USER_NAME
	mkdir /home/$NEW_USER_NAME/.ssh/
	touch /home/$NEW_USER_NAME/.ssh/authorized_keys
	echo "$USER_SSH_KEY" >> /home/$NEW_USER_NAME/.ssh/authorized_keys
	mkdir /home/$NEW_USER_NAME/scripts/

	# Установка утилит
	apt -y install sudo iptables-persistent chrony members

	# Конфигурация даты и времени
	timedatectl set-timezone GMT
	systemctl enable chrony

	# Настройка фаерволла (iptables)
	echo "#!/bin/bash

export WAN=$WAN
export WAN_IP=$WAN_IP
export WAN_PORT=$WAN_PORT

iptables -F
iptables -F -t nat
iptables -F -t mangle
iptables -X
iptables -t nat -X
iptables -t mangle -X

iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT
iptables -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT
iptables -A INPUT -p icmp --icmp-type time-exceeded -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT

iptables -A OUTPUT -o $WAN -j ACCEPT

iptables -A INPUT -p all -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -p all -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -p all -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

iptables -I FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

iptables -A INPUT -m state --state INVALID -j DROP
iptables -A FORWARD -m state --state INVALID -j DROP

iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP
iptables -A OUTPUT -p tcp ! --syn -m state --state NEW -j DROP

iptables -A INPUT -i $WAN -p tcp --dport $WAN_PORT -j ACCEPT

/sbin/iptables-save > /etc/iptables/rules.v4" > /etc/iptables.sh
	chmod +x /etc/iptables.sh && bash /etc/iptables.sh

	# Включение forwarding'a
	if grep -q net.ipv4.ip_forward /etc/sysctl.conf
	then
		echo "$(sed 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf)" > /etc/sysctl.conf
		echo "$(sed 's/#net.ipv4.ip_forward=0/net.ipv4.ip_forward=1/g' /etc/sysctl.conf)" > /etc/sysctl.conf
		echo "$(sed 's/net.ipv4.ip_forward=0/net.ipv4.ip_forward=1/g' /etc/sysctl.conf)" > /etc/sysctl.conf
	else
		echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
	fi

	# Настройка истории
	echo "export HISTSIZE=10000" >> ~/.bashrc
	echo "export HISTTIMEFORMAT='%h %d %H:%M:%S '" >> ~/.bashrc
	echo "PROMPT_COMMAND='history -a'" >> ~/.bashrc
	echo "export HISTIGNORE='ls:ll:history:w:htop'" >> ~/.bashrc
	source ~/.bashrc

	# Завершение настройки
	clear
	echo ""
	echo "Настройка сервера успешно завершена!"
	echo "Новый пользователь для подключения по SSH: $NEW_USER_NAME:$NEW_USER_PASSWORD"
	echo ""
	echo "Для корректной работы необходима перезагрузка сервера."
	read -p "Нажмите ENTER чтобы перезагрузить сервер и закончить настройку."
	shutdown -r now
}

function manageMenu() {
	echo ""
	echo "Добро пожаловать в скрипт для начальной настройки сервера!"
	echo ""
	echo "Похоже, что сервер уже настроен."
	echo ""
	echo "Что вы хотите сделать?"
	echo "   1) Добавить новый SSH ключ"
	echo "   2) Выйти"
	until [[ $MENU_OPTION =~ ^[1-2]$ ]]; do
		read -rp "Выберите вариант [1-2]: " MENU_OPTION
	done
	case $MENU_OPTION in
	1)
		addSSHkey
		;;
	2)
		exit 0
		;;
	esac
}

initialCheck

if grep -q initialsetup /etc/group; then
	manageMenu
else
	serverSetup
fi
