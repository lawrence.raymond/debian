#!/bin/bash

function isRoot() {
	if [ "$EUID" -ne 0 ]; then
		return 1
	fi
}

function checkOS() {
	source /etc/os-release
	if [[ $ID != "debian" && $VERSION_ID != 11 ]]; then
		return 1
	fi
}
			
function initialCheck() {
	if ! isRoot; then
		echo ""
		echo "Пожалуйста, запустите этот скрипт как root!"
		echo ""
		exit 1
	fi
	if ! checkOS; then
		echo ""
		echo "Ваша операционная система система не поддерживается!"
		echo "Скрипт создан для ОС Debian 11!"
		echo ""
		exit 1
	fi
}


function installQuestions() {
	clear
	echo ""
	echo "Добро пожаловать в скрипт для установки прокси сервера Dante!"
	echo "Пожалуйста, ответьте на следующие вопросы, чтобы продолжить."
	echo ""
	
	WAN=$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)
	read -rp "Введите название сетевого интерфейса Вашего сервера: " -e -i "${WAN}" WAN
	
	until [[ ${DANTE_PORT} =~ ^[0-9]+$ ]] && [ "${DANTE_PORT}" -ge 1 ] && [ "${DANTE_PORT}" -le 65535 ]; do
	read -rp "Введите порт для Dante сервера [1-65535]: " -e -i "25950" DANTE_PORT
	done
}

function newClient() {
	clear
	if grep -q danteinstall /etc/group; then
	echo ""
	echo "Текущие клиенты прокси: $(members danteinstall)"
	fi
	echo ""
	NEW_CLIENT_NAME=$(</dev/urandom tr -dc a-z | head -c 8)
	read -rp "Введите имя для нового клиента прокси: " -e -i "${NEW_CLIENT_NAME}" NEW_CLIENT_NAME
	
	NEW_CLIENT_PASSWORD=$(</dev/urandom tr -dc a-z-A-Z-0-9 | head -c 16)
	read -rp "Введите пароль для нового клиента прокси: " -e -i "${NEW_CLIENT_PASSWORD}" NEW_CLIENT_PASSWORD
	
	groupadd danteinstall
	useradd -G danteinstall -m -p $(perl -e 'print crypt($ARGV[0], "password")' $NEW_CLIENT_PASSWORD) -s /bin/bash $NEW_CLIENT_NAME
	mkdir /home/$NEW_CLIENT_NAME/.ssh/
	touch /home/$NEW_CLIENT_NAME/.ssh/authorized_keys
	
	clear
	echo ""
	echo "Новый клиент успешно создан!"
	echo "Данные для подключения: $NEW_CLIENT_NAME:$NEW_CLIENT_PASSWORD"
	echo ""
}

function serverSetup() {
	installQuestions
	
	# Обновляем систему
	apt -y update
	apt -y upgrade
	apt -y full-upgrade
	apt -y autoremove
	
	# Устанавливаем сервер
	apt -y install dante-server

	# Сохраним оригинальный конфиг
	mv /etc/danted.conf /etc/danted.conf.bak
	
	echo "user.privileged: root
user.unprivileged: nobody

internal: eth0 port = $DANTE_PORT
external: eth0

logoutput: /var/log/socks.log
errorlog: /var/log/sockd_err.log

socksmethod: username none #rfc931

client pass {
from: 0.0.0.0/0 to: 0.0.0.0/0
log: connect disconnect iooperation
}

socks pass {
from: 0.0.0.0/0 to: 0.0.0.0/0
command: connect udpassociate
log: connect disconnect iooperation
}" > /etc/danted.conf

	# Добавляем клиента
	newClient
	
	# Перегагружаем сервер и добавляем в автозагрузку
	systemctl restart danted 
	systemctl enable danted > /dev/null 2>&1
	
	# Открываем порты в iptables
	sed -i "\$iiptables -A INPUT -i $WAN -p tcp --dport $DANTE_PORT -j ACCEPT\niptables -A INPUT -i $WAN -p udp --dport $DANTE_PORT -j ACCEPT\n" /etc/iptables.sh
	bash /etc/iptables.sh

}

function manageMenu() {
	echo ""
	echo "Добро пожаловать в скрипт для установки прокси сервера Dante!"
	echo ""
	echo "Похоже, что сервер уже установлен."
	echo ""
	echo "Что вы хотите сделать?"
	echo "   1) Добавить нового клиента"
	echo "   2) Выйти"
	until [[ $MENU_OPTION =~ ^[1-2]$ ]]; do
		read -rp "Выберите вариант [1-2]: " MENU_OPTION
	done
	case $MENU_OPTION in
	1)
		newClient
		;;
	2)
		exit 0
		;;
	esac
}

initialCheck

if grep -q danteinstall /etc/group; then
	manageMenu
else
	serverSetup
fi
