#!/bin/bash

function isRoot() {
	if [ "$EUID" -ne 0 ]; then
		return 1
	fi
}

function checkOS() {
	source /etc/os-release
	if [[ $ID != "debian" && $VERSION_ID != 11 ]]; then
		return 1
	fi
}
			
function initialCheck() {
	if ! isRoot; then
		echo ""
		echo -e "Пожалуйста, запустите этот скрипт как root!"
		echo ""
		exit 1
	fi
	if ! checkOS; then
		echo ""
		echo -e "Ваша операционная система система не поддерживается!"
		echo -e "Скрипт создан для ОС Debian 11!"
		echo ""
		exit 1
	fi
}

function installQuestions() {
	clear
	echo ""
	echo -e "Добро пожаловать в установщик WireGuard!"
	echo -e "Пожалуйста, ответьте на следующие вопросы, чтобы продолжить."
	echo ""
	
	WAN=$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)
	read -rp "Введите название сетевого интерфейса Вашего сервера: " -e -i "${WAN}" WAN
	
	until [[ ${WG_PORT} =~ ^[0-9]+$ ]] && [ "${WG_PORT}" -ge 1 ] && [ "${WG_PORT}" -le 65535 ]; do
	read -rp "Введите порт для WireGuard сервера [1-65535]: " -e -i "51820" WG_PORT
	done
}

function newClient() {
	WAN_IP=$(ip -4 addr | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p' | awk '{print $1}' | head -1)
	read -rp "Введите IP Вашего сервера: " -e -i "${WAN_IP}" WAN_IP
	
	until [[ ${WG_PORT} =~ ^[0-9]+$ ]] && [ "${WG_PORT}" -ge 1 ] && [ "${WG_PORT}" -le 65535 ]; do
	read -rp "Введите порт для WireGuard сервера [1-65535]: " -e -i "51820" WG_PORT
	done
	
	read -rp "Введите имя нового WireGuard клиента: " -e -i "client001" CLIENTNAME
	cd /etc/wireguard
	wg genkey | tee $CLIENTNAME-private.key | wg pubkey > $CLIENTNAME-public.key
	
	echo "[Interface]
Address = 10.66.66.2/32
PrivateKey = $(cat $CLIENTNAME-private.key)
DNS = 208.67.222.222,208.67.220.220

[Peer]
PublicKey = $(cat /etc/wireguard/wg-public.key)
AllowedIPs = 0.0.0.0/0,::/0
Endpoint = $WAN_IP:$WG_PORT" >> /etc/wireguard/$CLIENTNAME-wg0.conf

	echo "[Peer]
PublicKey = $(cat $CLIENTNAME-public.key)
AllowedIPs = 10.66.66.2/32" >> "/etc/wireguard/wg0.conf"
	
	systemctl restart wg-quick@wg0 > /dev/null 2>&1
	
	clear
	echo ""
	echo "Новый клиент успешно создан!"
	echo "Содержимое конфига $CLIENTNAME для подключения:"
	echo ""
	echo ""
	echo "$(cat /etc/wireguard/$CLIENTNAME-wg0.conf)"
	echo ""
	echo ""
}

function installWireGuard() {
	installQuestions
	
	# Обновляем систему
	apt -y update
	apt -y upgrade
	apt -y full-upgrade
	apt -y autoremove
	
	# Устанавливаем WireGuard сервер
	apt -y install wireguard
	
	# Генерация ключей сервера
	cd /etc/wireguard
	umask 077
	wg genkey > wg-private.key
	wg pubkey < wg-private.key > wg-public.key
	
	echo "[Interface]
PrivateKey = $(cat wg-private.key)
Address = 10.66.66.1/24
SaveConfig = true
PostUp = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o $WAN -j MASQUERADE
PostUp = iptables -A INPUT -p icmp --icmp-type echo-request -j DROP
PostUp = iptables -A INPUT -i $WAN -p icmp --icmp-type echo-request -j DROP
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o $WAN -j MASQUERADE
PostDown = iptables -A INPUT -p icmp --icmp-type echo-request -j DROP
PostDown = iptables -A INPUT -i $WAN -p icmp --icmp-type echo-request -j DROP
ListenPort = $WG_PORT" > /etc/wireguard/wg0.conf

	newClient
	
	systemctl enable wg-quick@wg0 > /dev/null 2>&1
	systemctl start wg-quick@wg0 > /dev/null 2>&1
}

function manageMenu() {
	echo ""
	echo "Добро пожаловать в установщик WireGuard!"
	echo ""
	echo "Похоже, что WireGuard уже установлен."
	echo ""
	echo "Что вы хотите сделать?"
	echo "   1) Добавить нового клиента"
	echo "   2) Выйти"
	until [[ $MENU_OPTION =~ ^[1-2]$ ]]; do
		read -rp "Выберите вариант [1-2]: " MENU_OPTION
	done
	case $MENU_OPTION in
	1)
		newClient
		;;
	2)
		exit 0
		;;
	esac
}

initialCheck

if [[ -e /etc/wireguard/wg0.conf ]]; then
	manageMenu
else
	installWireGuard
fi
