#!/bin/bash

function isRoot() {
	if [ "$EUID" -ne 0 ]; then
		return 1
	fi
}

function checkOS() {
	source /etc/os-release
	if [[ $ID != "debian" && $VERSION_ID != 11 ]]; then
		return 1
	fi
}
			
function initialCheck() {
	if ! isRoot; then
		echo ""
		echo "Пожалуйста, запустите этот скрипт как root!"
		echo ""
		exit 1
	fi
	if ! checkOS; then
		echo ""
		echo "Ваша операционная система система не поддерживается!"
		echo "Скрипт создан для ОС Debian 11!"
		echo ""
		exit 1
	fi
}

function installQuestions() {
	clear
	echo ""
	echo -e "Добро пожаловать в установщик OpenVPN!"
	echo -e "Пожалуйста, ответьте на следующие вопросы, чтобы продолжить."
	echo ""
	
	WAN_IP=$(ip -4 addr | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p' | awk '{print $1}' | head -1)
	read -rp "Введите IP Вашего сервера: " -e -i "${WAN_IP}" WAN_IP
	
	WAN=$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)
	read -rp "Введите название сетевого интерфейса Вашего сервера: " -e -i "${WAN}" WAN
	
	until [[ ${OVPN_PORT} =~ ^[0-9]+$ ]] && [ "${OVPN_PORT}" -ge 1 ] && [ "${OVPN_PORT}" -le 65535 ]; do
	read -rp "Введите порт для OpenVPN сервера [1-65535]: " -e -i "22" OVPN_PORT
	done
}

function newClient() {
	read -rp "Введите имя нового OpenVPN клиента: " -e -i "client001" CLIENTNAME
	
	# Генерируем ключи клиента
	cd /usr/share/easy-rsa/
	./easyrsa build-client-full $CLIENTNAME nopass
	
	# Создаем директорию для клиента
	mkdir /etc/openvpn/server/clients/
	mkdir /etc/openvpn/server/clients/$CLIENTNAME
	cd /etc/openvpn/server/clients/$CLIENTNAME
	
	# Копируемключи и сертификаты, необходимые для подключения
	cp /usr/share/easy-rsa/pki/ca.crt .
	cp /usr/share/easy-rsa/pki/issued/$CLIENTNAME.crt .
	cp /usr/share/easy-rsa/pki/private/$CLIENTNAME.key .
	
	echo "client
dev tun
proto tcp
remote $WAN_IP $OVPN_PORT
auth-user-pass

cipher AES-256-CBC
persist-key
persist-tun

<ca>
$(cat /etc/openvpn/server/ca.crt)
</ca>

<cert>
$(cat /etc/openvpn/server/clients/$CLIENTNAME/$CLIENTNAME.crt)
</cert>

<key>
$(cat /etc/openvpn/server/clients/$CLIENTNAME/$CLIENTNAME.key)
</key>" > /etc/openvpn/server/clients/$CLIENTNAME/$CLIENTNAME.ovpn

	# Добавляем пользователя в user.pass
	NEW_USER_NAME=$(</dev/urandom tr -dc a-z | head -c 8)
	read -rp "Введите имя для нового пользователя: " -e -i "${NEW_USER_NAME}" NEW_USER_NAME
	
	NEW_USER_PASSWORD=$(</dev/urandom tr -dc a-z-A-Z-0-9 | head -c 16)
	read -rp "Введите пароль для нового пользователя: " -e -i "${NEW_USER_PASSWORD}" NEW_USER_PASSWORD
	
	echo "$NEW_USER_NAME:$NEW_USER_PASSWORD" >> /etc/openvpn/server/user.pass
	
	# Перезагружаем OpenVPN сервер
	systemctl restart openvpn-server@server /dev/null 2>&1
	
	# Смотрим данные для подключения для подключения
	clear
	echo ""
	echo "Новый клиент успешно создан!"
	echo ""
	echo "Логин и пароль для подключения: $NEW_USER_NAME:$NEW_USER_PASSWORD"
	echo "Содержимое конфига $CLIENTNAME для подключения:"
	echo ""
	echo ""
	echo "$(cat /etc/openvpn/server/clients/$CLIENTNAME/$CLIENTNAME.ovpn)"
	echo ""
	echo ""
}

function installOpenVPN() {
	installQuestions
	
	# Обновляем систему
	apt -y update
	apt -y upgrade
	apt -y full-upgrade
	apt -y autoremove
	
	# Устанавливаем необходимые компоненты
	apt -y install openvpn easy-rsa
	
	# Генерация ключей и сертификатов сервера
	cd /usr/share/easy-rsa
	./easyrsa init-pki
	./easyrsa build-ca nopass
	./easyrsa gen-dh
	./easyrsa build-server-full server nopass
	
	# Копирование ключей и сертификатов сервера
	cd /etc/openvpn/server
	cp /usr/share/easy-rsa/pki/issued/server.crt .
	cp /usr/share/easy-rsa/pki/private/server.key .
	cp /usr/share/easy-rsa/pki/ca.crt .
	cp /usr/share/easy-rsa/pki/dh.pem .
	
	echo "dev tun
proto tcp
server 10.8.0.0 255.255.255.0
port $OVPN_PORT

auth-user-pass-verify /etc/openvpn/server/verify.sh via-file

ifconfig-pool-persist /etc/openvpn/server/ipp.txt
client-config-dir /etc/openvpn/ccd-users-udp

username-as-common-name
tmp-dir /etc/openvpn/server/tmp
script-security 2

cert server.crt
key server.key
ca ca.crt
dh dh.pem
" > /etc/openvpn/server/server.conf

	echo 'push "redirect-gateway def1"
push "dhcp-option DNS 1.1.1.1"
' >> /etc/openvpn/server/server.conf

	echo "cipher AES-256-CBC
daemon
persist-key
persist-tun
keepalive 10 120

status /etc/openvpn/status-users-udp.log
log log.log" >> /etc/openvpn/server/server.conf
	
	# Скрипт проверки логина и пароля
	touch /etc/openvpn/server/verify.sh
	echo '#!/bin/sh' >> /etc/openvpn/server/verify.sh
	echo '## format: username:password username:password ...' >> /etc/openvpn/server/verify.sh
	echo '## you can even have same usernames with different passwords' >> /etc/openvpn/server/verify.sh
	echo '# USERS=`user1:pass1 user2:pass2 user3:pass3`' >> /etc/openvpn/server/verify.sh
	echo '## you could put username:password in' >> /etc/openvpn/server/verify.sh
	echo '## a separate file and read it like this' >> /etc/openvpn/server/verify.sh
	echo 'USERS=`cat /etc/openvpn/server/user.pass`' >> /etc/openvpn/server/verify.sh
	echo 'vpn_verify() {' >> /etc/openvpn/server/verify.sh
	echo 'if [ ! $1 ] || [ ! $2 ]; then' >> /etc/openvpn/server/verify.sh
	echo '#echo "No username or password: $*"' >> /etc/openvpn/server/verify.sh
	echo 'exit 1' >> /etc/openvpn/server/verify.sh
	echo 'fi' >> /etc/openvpn/server/verify.sh
	echo '## it can also be done with grep or sed' >> /etc/openvpn/server/verify.sh
	echo 'for i in $USERS; do' >> /etc/openvpn/server/verify.sh
	echo 'if [ "$i" = "$1:$2" ]; then' >> /etc/openvpn/server/verify.sh
	echo '## you can add here logging of users' >> /etc/openvpn/server/verify.sh
	echo '## if you have enough space for log file' >> /etc/openvpn/server/verify.sh
	echo '#echo `date` $1:$2 >> your_log_file' >> /etc/openvpn/server/verify.sh
	echo 'exit 0' >> /etc/openvpn/server/verify.sh
	echo 'fi' >> /etc/openvpn/server/verify.sh
	echo 'done' >> /etc/openvpn/server/verify.sh
	echo '}' >> /etc/openvpn/server/verify.sh
	echo 'if [ ! $1 ] || [ ! -e $1 ]; then' >> /etc/openvpn/server/verify.sh
	echo '#echo "No file"' >> /etc/openvpn/server/verify.sh
	echo 'exit 1' >> /etc/openvpn/server/verify.sh
	echo 'fi' >> /etc/openvpn/server/verify.sh
	echo '## $1 is file name which contains' >> /etc/openvpn/server/verify.sh
	echo '## passed username and password' >> /etc/openvpn/server/verify.sh
	echo 'vpn_verify `cat $1`' >> /etc/openvpn/server/verify.sh
	echo '#echo "No user with this password found"' >> /etc/openvpn/server/verify.sh
	chmod +x verify.sh

	# Директория для временного скрипта аутентификации
	mkdir /etc/openvpn/server/tmp
	mkdir /etc/openvpn/ccd-users-udp
	
	# Пользователи и пароли
	touch /etc/openvpn/server/user.pass
	
	# Настройка iptables
	sed -i "\$iiptables -t nat -I POSTROUTING 1 -s 10.8.0.0/24 -o $WAN -j MASQUERADE\niptables -I INPUT 1 -i tun0 -j ACCEPT\niptables -I FORWARD 1 -i $WAN -o tun0 -j ACCEPT\niptables -I FORWARD 1 -i tun0 -o $WAN -j ACCEPT\niptables -I INPUT 1 -i $WAN -p tcp --dport $OVPN_PORT -j ACCEPT\n" /etc/iptables.sh
	bash /etc/iptables.sh
	
	# Добавляем в автозагрузку и запускаем
	systemctl enable openvpn-server@server.service
	systemctl start openvpn-server@server.service
	
	# Добавляем клиента
	newClient
}

function manageMenu() {
	echo ""
	echo "Добро пожаловать в установщик OpenVPN!"
	echo ""
	echo "Похоже, что OpenVPN уже установлен."
	echo ""
	echo "Что вы хотите сделать?"
	echo "   1) Добавить нового клиента"
	echo "   2) Выйти"
	until [[ $MENU_OPTION =~ ^[1-2]$ ]]; do
		read -rp "Выберите вариант [1-2]: " MENU_OPTION
	done
	case $MENU_OPTION in
	1)
		newClient
		;;
	2)
		exit 0
		;;
	esac
}

initialCheck

if [[ -e /etc/openvpn/server/server.conf ]]; then
	manageMenu
else
	installOpenVPN
fi
