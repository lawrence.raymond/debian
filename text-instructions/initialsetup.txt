## Обновление системы и пакетов
apt -y update
apt -y upgrade
apt -y full-upgrade
apt -y autoremove

## Настройка ssh
nano /etc/ssh/sshd_config

# Изменяем там следующие параметры 
Port 25431 (или любой другой)
PermitRootLogin no
PasswordAuthentication no
#AcceptEnv LANG LC_* (эту строку комментируем)

# Перезапускаем службу sshd
systemctl restart sshd

# Добавляем нового судо пользователя (назовём его, например kopfty, если выбрали другое имя - не забывайте менять в следующих командах)
adduser kopfty
usermod -aG sudo kopfty

# Создаем директорию .ssh в домашней папке нового пользователя
mkdir /home/kopfty/.ssh/

# Теперь копируем свой ssh ключ и вставляем в authorized_keys
nano /home/kopfty/.ssh/authorized_keys

# Устанавливаем утилиты (в менюшках, которые появляются при устиановке, принимаем и соглашаемся на всё)
apt -y install sudo iptables-persistent net-tools htop iftop tzdata ntp members

# Конфигурация даты и времени
dpkg-reconfigure tzdata

# Настройка фаерволла (iptables)
systemctl enable netfilter-persistent.service
systemctl start netfilter-persistent.service

nano /etc/iptables.sh

-----СОДЕРЖИМОЕ /etc/iptables.sh (не забудьте изменить переменные под ваши значения)-----
#!/bin/bash

export WAN=eth0
export WAN_IP=48.198.16.125
export WAN_PORT=25431

iptables -F
iptables -F -t nat
iptables -F -t mangle
iptables -X
iptables -t nat -X
iptables -t mangle -X

iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT
iptables -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT
iptables -A INPUT -p icmp --icmp-type time-exceeded -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT

iptables -A OUTPUT -o $WAN -j ACCEPT

iptables -A INPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -p all -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -I FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

iptables -A INPUT -m state --state INVALID -j DROP
iptables -A FORWARD -m state --state INVALID -j DROP

iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP
iptables -A OUTPUT -p tcp ! --syn -m state --state NEW -j DROP

iptables -A INPUT -i $WAN -p tcp --dport $WAN_PORT -j ACCEPT
iptables -A INPUT -i $WAN -p udp --dport 123 -j ACCEPT

/sbin/iptables-save > /etc/iptables/rules.v4"

-----СОДЕРЖИМОЕ /etc/iptables.sh (не забудьте изменить переменные под ваши значения)-----

# Делаем файл исполняемым и запускаем
chmod +x /etc/iptables.sh && bash /etc/iptables.sh

# Проверяем применились ли правила
iptables -nvL

# Включаем переадресацию (добавляем строку net.ipv4.ip_forward=1)
nano /etc/sysctl.conf

# Настройка истории
echo "export HISTSIZE=10000" >> ~/.bashrc
echo "export HISTTIMEFORMAT='%h %d %H:%M:%S '" >> ~/.bashrc
echo "PROMPT_COMMAND='history -a'" >> ~/.bashrc
echo "export HISTIGNORE='ls:ll:history:w:htop'" >> ~/.bashrc
source ~/.bashrc

# Перезагружаем сервер, при подключении заново - используем нового пользователя и наш новый порт
