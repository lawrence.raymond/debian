#!/bin/bash

function isRoot() {
	if [ "$EUID" -ne 0 ]; then
		return 1
	fi
}

function checkOS() {
	source /etc/os-release
	if [[ $ID != "debian" && $VERSION_ID != 11 ]]; then
		return 1
	fi
}

function initialCheck() {
	if ! isRoot; then
		echo ""
		echo -e "Пожалуйста, запустите этот скрипт как root!"
		echo ""
		exit 1
	fi
	if ! checkOS; then
		echo ""
		echo -e "Ваша операционная система система не поддерживается!"
		echo -e "Скрипт создан для ОС Debian 11!"
		echo ""
		exit 1
	fi
}

function installQuestions() {
	clear
	echo ""
	echo -e "Добро пожаловать в скрипт для установки stunnel!"
	echo -e "Пожалуйста, ответьте на следующие вопросы, чтобы продолжить."
	echo ""

	WAN_IP=$(ip -4 addr | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p' | awk '{print $1}' | head -1)
	read -rp "Введите IP Вашего сервера: " -e -i "${WAN_IP}" WAN_IP

	WAN=$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)
	read -rp "Введите название сетевого интерфейса Вашего сервера: " -e -i "${WAN}" WAN
}

function removeStunnel() {
	clear
	echo "Полное удаление stunnel"

	# Остановка службы stunnel4
	systemctl stop stunnel4

	# Удаление службы systemd stunnel4
	systemctl disable stunnel4
	rm /etc/systemd/system/stunnel4.service
	systemctl daemon-reload

	# Удаление пакета stunnel4
	apt remove --purge -y stunnel4

	# Удаление директорий с клиентскими конфигурациями
	rm -rf /etc/stunnel/

	# Удаление логов
	rm -rf /var/log/stunnel/

	echo "stunnel успешно удалён."
}

function removeClient() {
	clear
	echo "Удаление клиента"

	local client_dirs="/etc/stunnel/clients/*"
	local count=0
	local client_list=()  # Массив для хранения имен клиентов

	for client_dir in $client_dirs; do
		if [[ -d "$client_dir" ]]; then
			count=$((count+1))
			client_name=$(basename "$client_dir")
			client_list+=("$client_name")
			echo "   $count) $client_name"
		fi
	done

	if [[ $count -eq 0 ]]; then
		echo "Нет существующих клиентов."
		return
	fi

	local choice
	read -rp "Выберите номер клиента для удаления [1-$count]: " choice

	if [[ ! "$choice" =~ ^[0-9]+$ || "$choice" -lt 1 || "$choice" -gt $count ]]; then
		echo "Некорректный выбор. Введите номер клиента от 1 до $count."
		return
	fi

	local client_to_remove="${client_list[choice-1]}"

	# Удаление директории клиента и связанных файлов
	rm -rf "/etc/stunnel/clients/$client_to_remove"

	# Удаление правила iptables
	sed -i "/-A INPUT -i $WAN -p tcp --dport ${CLIENT_PORT} -j ACCEPT/d" /etc/iptables.sh
	bash /etc/iptables.sh  # Применяем изменения правил iptables

	# Удаление из конфига stunnel.conf
	sed -i "/\[$client_to_remove\]/,/^$/d" /etc/stunnel/stunnel.conf

	systemctl stop stunnel4
	systemctl start stunnel4

	echo "Клиент '$client_to_remove' успешно удален."
}

function showClients() {
	clear
	echo "Список существующих клиентов:"

	local client_dirs="/etc/stunnel/clients/*"
	local count=0

	for client_dir in $client_dirs; do
		if [[ -d "$client_dir" ]]; then
			count=$((count+1))
			client_name=$(basename "$client_dir")
			echo "   $count) $client_name"
		fi
	done

	if [[ $count -eq 0 ]]; then
		echo "Нет существующих клиентов."
	fi
}

function newClient() {

	WAN_IP=$(ip -4 addr | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p' | awk '{print $1}' | head -1)
	read -rp "Введите IP Вашего сервера: " -e -i "${WAN_IP}" WAN_IP

	WAN=$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)
	read -rp "Введите название сетевого интерфейса Вашего сервера: " -e -i "${WAN}" WAN

	while true; do
	read -rp "Введите имя нового stunnel клиента: " -e -i "" CLIENTNAME

	# Проверяем, существует ли директория с именем клиента
	if [[ -d "/etc/stunnel/clients/$CLIENTNAME" ]]; then
		echo "Клиент с именем '$CLIENTNAME' уже существует. Пожалуйста, используйте другое имя."
	else
		break  # Если имя уникальное, выходим из цикла
	fi
	done

	until [[ ${CLIENT_PORT} =~ ^[0-9]+$ ]] && [ "${CLIENT_PORT}" -ge 25601 ] && [ "${CLIENT_PORT}" -le 25699 ]; do
	read -rp "Введите порт для нового stunnel клиента [25601-25699]: " -e -i "" CLIENT_PORT
	done

	read -rp "Введите адрес назначения нового stunnel клиента (ip:port): " -e -i "" CLIENTDESTINATION

	# Создаём ключи и сертификаты клиентов
	cd /etc/stunnel
	openssl req -newkey rsa:2048 -nodes -keyout $CLIENTNAME-desktop.key \
	-x509 -days 3650 -subj "/CN=$CLIENTNAME-desktop" \
	-out $CLIENTNAME-desktop.crt

	openssl req -newkey rsa:2048 -nodes -keyout $CLIENTNAME-mobile.key \
	-x509 -days 3650 -subj "/CN=$CLIENTNAME-mobile" \
	-out $CLIENTNAME-mobile.crt

	openssl pkcs12 -export -in $CLIENTNAME-mobile.crt \
	-inkey $CLIENTNAME-mobile.key -out $CLIENTNAME-mobile.p12

	# Записываем ключи и сертификаты клиентов в clients.pem
	echo "# $CLIENTNAME-desktop" >> clients.pem && cat $CLIENTNAME-desktop.crt >> clients.pem && echo "# $CLIENTNAME-mobile" >> clients.pem && cat $CLIENTNAME-mobile.crt >> clients.pem

	mkdir /etc/stunnel/clients/$CLIENTNAME
	cp /etc/stunnel/{$CLIENTNAME*,stunnel-server.crt} /etc/stunnel/clients/$CLIENTNAME
	rm -f /etc/stunnel/CLIENTNAME*

	sed -i "\$iiptables -A INPUT -i $WAN -p tcp --dport $CLIENT_PORT -j ACCEPT\n" /etc/iptables.sh
	bash /etc/iptables.sh

	echo "[$CLIENTNAME]
accept = $WAN_IP:$CLIENT_PORT
connect = $CLIENTDESTINATION
renegotiation = no
ciphers = ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-RSA-AES256-SHA
cert = /etc/stunnel/stunnel-server.crt
key = /etc/stunnel/stunnel-server.key
CAfile = /etc/stunnel/clients.pem
verifyPeer = yes
" >> /etc/stunnel/stunnel.conf

	echo "[$CLIENTNAME]
client = yes
accept = 
connect = $WAN_IP:$CLIENT_PORT
verifyPeer = yes
CAfile = /etc/stunnel/stunnel-server.crt
cert = /etc/stunnel/$CLIENTNAME-desktop.crt
key = /etc/stunnel/$CLIENTNAME-desktop.key" > /etc/stunnel/clients/$CLIENTNAME/stunnel.conf

	systemctl restart stunnel4
}


function serverSetup () {
	installQuestions

	# Обновляем систему
	apt -y update
	apt -y upgrade
	apt -y full-upgrade
	apt -y autoremove

	# Устанавливаем необходимые пакеты
	apt -y install stunnel4

	# Редактируем stunnel.conf
	echo "chroot = /var/lib/stunnel4
pid = /stunnel4.pid
setuid = stunnel4
setgid = stunnel4

debug = 0

socket = l:TCP_NODELAY=1
socket = r:TCP_NODELAY=1

curve = secp521r1
sslVersion = all
options = NO_SSLv2
options = NO_SSLv3
" > /etc/stunnel/stunnel.conf

	# Создаём ключи и сертификаты сервера
	cd /etc/stunnel/
	openssl req -newkey rsa:2048 -nodes -keyout stunnel-server.key \
	-x509 -days 3650 -subj "/CN=stunnel-server" \
	-out stunnel-server.crt

	# Запускаем stunnel
	systemctl enable stunnel4
	systemctl start stunnel4

	# Создаём директорию клиентских для конфигов
	mkdir /etc/stunnel/clients

	# Завершение установки
	clear
	echo ""
	echo "Установка stunnel успешно завершена!"
	echo "Все клиентские файлы находятся в /etc/stunnel/clients"
	echo ""
}

function manageMenu() {
	clear
	echo ""
	echo "Добро пожаловать в скрипт для установки stunnel!"
	echo "Похоже, что stunnel уже установлен."
	echo ""
	echo "Что вы хотите сделать?"
	echo "   1) Добавить нового клиента"
	echo "   2) Просмотреть список клиентов"
	echo "   3) Удалить клиента"
	echo "   4) Удалить stunnel и все связанные файлы"
	echo "   5) Выйти"
	until [[ $MENU_OPTION =~ ^[1-5]$ ]]; do
		read -rp "Выберите вариант [1-5]: " MENU_OPTION
	done
	case $MENU_OPTION in
	1)
		newClient
		;;
	2)
		showClients
		;;
	3)
		removeClient
		;;
	4)
		removeStunnel
		;;
	5)
		exit 0
		;;
	esac
}

initialCheck

if [[ -e /etc/stunnel/stunnel.conf ]]; then
	manageMenu
else
	serverSetup
fi
